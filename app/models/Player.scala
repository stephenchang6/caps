package models

case class Player(
  val id: Long,
  val name: String,
)

case class PlayerData(
  val name: String,
)
