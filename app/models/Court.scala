package models

case class Court(
  val id: Long,
  val name: String,
  val timezone: String
) extends Crudable

case class CourtData(
  val name: String,
  val timezone: Option[String]
) extends CrudableData
