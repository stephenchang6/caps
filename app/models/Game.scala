package models

case class Game(
  id: Long,
  goodTeamId: Int,
  badTeamId: Int,
  scoreTo: Int,
  startingPlayerId: Int,
  acrossTeamId: Int,
  badTeamScore: Int,
  goodTeamScore: Int,
  courtId: Int
)

case class GameData(
  badPlayer1Id: Int,
  badPlayer2Id: Int,
  goodPlayer1Id: Int,
  goodPlayer2Id: Int,
  courtId: Int,
  scoreTo: Int,
  startingPlayerId: Int,
  badTeamAcross: Boolean
)
