package controllers

import javax.inject.Inject
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import models.{Player,PlayerData}
import dao.PlayerDAO
import views._

import scala.concurrent.{ExecutionContext, Future}

class PlayerController @Inject()(
  playerDao: PlayerDAO,
  val controllerComponents: ControllerComponents
)(
  implicit executionContext: ExecutionContext
) extends BaseController
  with play.api.i18n.I18nSupport {

  val playerForm = Form(
    mapping(
      "name" -> text
    )(PlayerData.apply)(PlayerData.unapply)
  )

  def index = Action.async { implicit request: Request[AnyContent] =>
    playerDao.all.map{ cs => Ok(views.html.players(playerForm, cs)) }
  }

  def create = Action.async { implicit request =>
    playerForm.bindFromRequest.fold(
      formWithErrors => {
        Future(BadRequest("ERROR"))
      },
      playerData => {
        playerDao.insert(playerData).map{id =>
          Redirect(routes.PlayerController.index)
        }
      }
    )
  }
}
