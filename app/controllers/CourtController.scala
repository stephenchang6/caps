package controllers

import javax.inject.Inject
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import models.{Court,CourtData}
import dao.CourtDAO
import views._

import scala.concurrent.{ExecutionContext, Future}

class CourtController @Inject()(
  courtDao: CourtDAO,
  val controllerComponents: ControllerComponents
)(
  implicit executionContext: ExecutionContext
) extends BaseController
  with play.api.i18n.I18nSupport {

  val courtForm = Form(
    mapping(
      "name" -> text,
      "timezone"  -> optional(text)
    )(CourtData.apply)(CourtData.unapply)
  )

  def index = Action.async { implicit request: Request[AnyContent] =>
    courtDao.all.map{ cs => Ok(views.html.courts(courtForm, cs)) }
  }

  def create = Action.async { implicit request =>
    courtForm.bindFromRequest.fold(
      formWithErrors => {
        Future(BadRequest("ERROR"))
      },
      courtData => {
        courtDao.insert(courtData).map{id =>
          Redirect(routes.CourtController.index)
        }
      }
    )
  }
}
