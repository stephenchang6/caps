package controllers

import javax.inject.Inject
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import models.{Game,GameData}
import dao.GameDAO
import views._

import scala.concurrent.{ExecutionContext, Future}

class GameController @Inject()(
  gameDao: GameDAO,
  val controllerComponents: ControllerComponents
)(
  implicit executionContext: ExecutionContext
) extends BaseController
  with play.api.i18n.I18nSupport {

  val gameForm = Form(
    mapping(
      "badPlayer1Id" -> number,
      "badPlayer2Id" -> number,
      "goodPlayer1Id" -> number,
      "goodPlayer2Id" -> number,
      "gameId" -> number,
      "scoreTo" -> number,
      "startingPlayerId" -> number,
      "badTeamAcross" -> boolean,
    )(GameData.apply)(GameData.unapply)
  )

  def index = Action.async { implicit request: Request[AnyContent] =>
    Future( Ok(views.html.newGame(gameForm)) )
  }

  def show(id: Long) = Action(Ok(id.toString))

  def create = Action.async { implicit request =>
    gameForm.bindFromRequest.fold(
      formWithErrors => {
        Future(BadRequest("ERROR"))
      },
      gameData => {
        gameDao.insert(gameData).map{id =>
          Redirect(routes.GameController.show(1))
        }
      }
    )
  }
}
