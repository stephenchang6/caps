package dao

import scala.concurrent.{ExecutionContext, Future}
import javax.inject.Inject

import models.{Court, CourtData}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

class CourtDAO @Inject()(
  protected val dbConfigProvider : DatabaseConfigProvider
)(
  implicit executionContext: ExecutionContext
) extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private val Courts = TableQuery[CourtsTable]

  def all:Future[Seq[Court]] = db.run(Courts.result)
  def insert(court:CourtData) = db.run{
    court match {
      case CourtData(name, Some(tz)) => Courts.map(c => (c.name, c.timezone)) returning Courts.map(_.id) += (name, tz)
      case CourtData(name, None) => Courts.map(_.name) returning Courts.map(_.id) += name
    }
  }

  private class CourtsTable(tag: Tag) extends Table[Court](tag, "courts"){
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
    def name = column[String]("NAME")
    def timezone = column[String]("TIME_ZONE")
    def * = (id, name, timezone) <> (Court.tupled, Court.unapply)
  }
}
