package dao

import scala.concurrent.{ExecutionContext, Future}
import javax.inject.Inject

import models.{Game, GameData}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

class GameDAO @Inject()(
  protected val dbConfigProvider : DatabaseConfigProvider
)(
  implicit executionContext: ExecutionContext
) extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private val Games = TableQuery[GamesTable]

  def all:Future[Seq[Game]] = db.run(Games.result)
  def insert(game:GameData) = {
    db.run(Games += Game(0, 1, 1, 1, 1, 1, 1, 1, 1))
  }

  private class GamesTable(tag: Tag) extends Table[Game](tag, "games"){
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
    def goodTeamId = column[Int]("GOOD_TEAM_ID")
    def badTeamId = column[Int]("BAD_TEAM_ID")
    def scoreTo = column[Int]("SCORE_TO")
    def startingPlayerId = column[Int]("STARTING_PLAYER_ID")
    def acrossTeamId = column[Int]("ACROSS_TEAM_ID")
    def goodTeamScore = column[Int]("GOOD_TEAM_SCORE")
    def badTeamScore = column[Int]("BAD_TEAM_SCORE")
    def courtId = column[Int]("COURT_ID")
    def * = (id, goodTeamId, badTeamId, scoreTo, startingPlayerId, acrossTeamId, goodTeamScore, badTeamScore, courtId) <> (Game.tupled, Game.unapply)
  }
}
