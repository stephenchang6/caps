package dao

import scala.concurrent.{ExecutionContext, Future}
import javax.inject.Inject

import models.{Player, PlayerData}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

class PlayerDAO @Inject()(
  protected val dbConfigProvider : DatabaseConfigProvider
)(
  implicit executionContext: ExecutionContext
) extends HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private val Players = TableQuery[PlayersTable]

  def all:Future[Seq[Player]] = db.run(Players.result)
  def insert(player:PlayerData) = db.run{
    Players.map(_.name) returning Players.map(_.id) += player.name
  }

  private class PlayersTable(tag: Tag) extends Table[Player](tag, "players"){
    def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)
    def name = column[String]("NAME")
    def * = (id, name) <> (Player.tupled, Player.unapply)
  }
}
