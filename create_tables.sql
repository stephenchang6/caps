create table players(
  id int unsigned auto_increment primary key,
  name varchar(255) not null,
  birthday date,
  female boolean
);

create table games(
  id int unsigned auto_increment primary key,
  good_team_id int not null,
  bad_team_id int not null,
  score_to int not null default 5,
  starting_player_id int,
  across_team_id int,
  bad_team_score int default 0,
  good_team_score int default 0,
  court_id int not null
);

create table teams(
  id int unsigned auto_increment primary key,
  left_player_id int not null,
  right_player_id int not null,
  good_guys boolean not null
);

create table shots(
  id int unsigned auto_increment primary key,
  made boolean not null,
  point boolean not null,
  rebuttal boolean not null,
  bounced boolean not null,
  out_of_turn boolean not null,
  player_id int not null,
  game_id int not null,
  team_id int not null,
  rawtimestamp datetime not null
);

create table courts(
  id int unsigned auto_increment primary key,
  name varchar(255) not null,
  time_zone varchar(255) not null default "UTC"
);
